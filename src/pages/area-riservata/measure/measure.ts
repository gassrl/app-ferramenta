import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { MeasureProvider } from '../../../providers/measure/measure';

import { LoginPage } from '../../login/login';
import { MeasureDetailPage } from '../measure-detail/measure-detail';

@IonicPage()
@Component({
  selector: 'page-measure',
  templateUrl: 'measure.html',
  providers: [MeasureProvider]
})
export class MeasurePage {
	
	showStaticPart: any;
	showMeasure: any;
	cambioFunzionalita: string = "dettagli";
	measure: any;
	loading: any;
	numberMeasure: any;
	
	predefinitaFormMisura: any;
	
	newMeasureForm: FormGroup;
	submitAttempt: boolean = false;
	
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public measureService: MeasureProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App, public storage: Storage) {		
		this.predefinitaFormMisura = "0";
		this.newMeasureForm = formBuilder.group({
      nome: ["", Validators.required],
      predefinita: [this.predefinitaFormMisura, Validators.required]
    });
	}

  ionViewWillEnter() {		
		this.showStaticPart = false;
		this.showMeasure = false;
		this.showLoader();
		this.getAllMeasure(false);
  }
	

	/* --- METODO CHE RITORNA TUTTE LE LISTE ATTIVE --- */
	getAllMeasure(openLast){
		this.measureService.getAllMeasure(openLast).then((result) => {
		  this.loading.dismiss();
			if(result != 0){
				this.showMeasure = true;
				this.measure = result;
				this.numberMeasure = result["length"];
				if(openLast){
					this.openMeasure(this.measure[0].ID,this.measure[0].Nome,this.measure[0].Predefinita)
				}
			}
			else{
				this.numberMeasure = 0;
			}
			this.showStaticPart = true;
		}, (err) => {
		  this.loading.dismiss();
			this.showStaticPart = true;
			if(err != "404 Nessuna unita di misura trovata"){
			  console.log(err);
				this.showAlert(err);
			}
		});
	}
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM PER AGGIUNGERE UNA LISTA
	newMeasureFormAction(){
    this.submitAttempt = true;
		if(this.newMeasureForm.valid){
			this.addMeasure(this.newMeasureForm.value);
			console.log("OK");
		}
    console.log(this.newMeasureForm.value);
	}
	
	/* --- METODO CHE AGGIUNGE UNA NUOVA LISTA --- */
	addMeasure(value){
		this.showLoader();
		this.measureService.addMeasure(value).then((result) => {
			this.newMeasureForm.reset();
			this.predefinitaFormMisura = "0"
    	this.submitAttempt = false;
			this.cambioFunzionalita = "dettagli";
			this.getAllMeasure(false);
		}, (err) => {
		  this.loading.dismiss();
			this.showAlert(err);
		});
	}

	//METODO CHE APRE LA SINGOLA LISTA
	openMeasure(ID,Nome,Predefinita){
		this.navCtrl.push(MeasureDetailPage,{
			measureID: ID,
			measureNome: Nome,
			measurePredefinita: Predefinita
		});
	}
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailMeasure(){
		this.cambioFunzionalita = "dettagli";
	}
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
		}
		else{
			title = "CONNESSIONE ASSENTE";
			content = "Verifica la connessione a internet";
		}
	  let confirm = this.alertCtrl.create({
      title: title,
      message: content,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.goHome();
          }
        }
      ]
    });
    confirm.present();
  }
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
}