import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { LoginPage } from '../../login/login';
import { ListsPage } from '../lists/lists';

import { ListsProvider } from '../../../providers/lists/lists';
import { MeasureProvider } from '../../../providers/measure/measure';

@IonicPage()
@Component({
  selector: 'page-list-detail',
  templateUrl: 'list-detail.html',
  providers: [ListsProvider,MeasureProvider]
})
export class ListDetailPage {
	
	listID: any;
	listNomeCliente: any;
	//listPagamento: any; RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
	listTarga: any;
	listData: any;
	listNomeUtente: any;
	listCognomeUtente: any;
	listNote: any;
	userRole: any;
	
	showFormRowList: boolean = false;
	showDetailListButton: boolean = false;
	
	rowID: any;
	rowProdotto: any;
	rowQuantita: any;
	
	rowMeasure: any;
	firstMeasure: any;
	oldRowMeasure: any;
	
	showStaticPart: any;
	loading: any;
	listRowDetail: any;
	showRow: any;
	cambioFunzionalita: string = "dettagli";
	
	modifyListForm: FormGroup;
	submitAttempt: boolean = false;
	nomeCliente: any;
	note: any;
	statoLista: any;
	
	addRowForm: FormGroup;
	submitAttemptAddRowForm: boolean = false;
	rowsNumber: any;
	
	measure: any;
	
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public listService: ListsProvider, public measureService: MeasureProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App) {
  	this.listID = navParams.get("listID");
  	this.listNomeCliente = navParams.get("nomeCliente");
  	this.listNote = navParams.get("note");
  	//this.listPagamento = navParams.get("pagamento"); RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
  	this.listTarga = navParams.get("targa");
  	this.listData = navParams.get("data");
  	this.listNomeUtente = navParams.get("nomeUtente");
  	this.listCognomeUtente = navParams.get("cognomeUtente");
  	this.userRole = navParams.get("userRole");
		
		if(this.userRole < 2){
			this.showDetailListButton = true;
		}
		
		this.rowID = "";
		this.rowProdotto = "";
		this.rowQuantita = "";
		this.rowMeasure = "";
		this.showRow = false;
		this.showStaticPart = true;	
		this.showLoader();
		this.statoLista = navParams.get("statoLista");
		
		this.modifyListForm = formBuilder.group({
			id: [this.listID],
      nomeCliente: [this.listNomeCliente, Validators.required],
      targa: [this.listTarga],
			//pagamaneto: [this.listPagamento, Validators.required], RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
      note: [this.listNote],
    });
		
		this.addRowForm = formBuilder.group({
      id: [this.rowID],
      prodotto: ["", Validators.required],
      quantita: ["", Validators.required],
			measure: [this.rowMeasure, Validators.required]
    });

		this.getAllMeasure();
  	this.getListRowDetail();
	}

	/* --- METODO CHE RITORNA TUTTE LE LISTE ATTIVE --- */
	getListRowDetail(){
		this.listService.getListRowsDetail(this.listID).then((result) => {
		  this.loading.dismiss();
			if(result != 0){
				this.listRowDetail = result;
				this.rowsNumber = result["length"];
				this.showRow = true;
			}
			else{
				this.rowsNumber = 0;
			}
			this.showStaticPart = true;
		}, (err) => {
		  this.loading.dismiss();
			if(err != "404 Nessuna riga trovata"){
			  console.log(err);
				this.showAlert(err);
			}
		});
	}
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM
	modifyListFormAction(){
    this.submitAttempt = true;
		if(this.modifyListForm.valid){
			this.modifyList(this.modifyListForm.value);
			console.log("OK");
		}
    console.log(this.modifyListForm.value);
	}
	
	//METODO CHE MODIFICA I DATI DELLA LISTA
	modifyList(params){
		this.showLoader();
		this.listService.modifyList(params).then((result) => {
			//this.modifyUserForm.reset();
      this.listNomeCliente = params.nomeCliente;
      this.listTarga = params.targa;
      //this.listPagamento = params.pagamaneto; RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
      this.listNote = params.note;
    	this.submitAttempt = false;
			this.showDetailList();
		  this.loading.dismiss();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM
	addRowFormAction(){
    this.submitAttemptAddRowForm = true;
		if(this.addRowForm.value.id != "" && this.addRowForm.value.id){
			if(this.addRowForm.value.prodotto != this.rowProdotto || this.addRowForm.value.quantita != this.rowQuantita || this.oldRowMeasure != this.rowMeasure){
				if(this.addRowForm.value.quantita <= 0){
					this.showConfirmAction(this.addRowForm.value);
				}
				else{
					console.log("VALORI DIVERSI");
					console.log(this.addRowForm.value);
					this.modifyRowList(this.addRowForm.value);
				}
			}
			else{
				console.log("VALORI UGUALI");
				this.rowID = "";
				this.rowProdotto = "";
				this.rowQuantita = "";
				this.rowMeasure = this.firstMeasure;	
				this.resetFormAddRowValue();
    		this.submitAttemptAddRowForm = false;
			}
		}
		else{
			if(this.addRowForm.valid && this.addRowForm.value.quantita > 0){
				this.addRowList(this.addRowForm.value);
				console.log("OK");
			}
		}
	}
	
	removeRowFormAction(){
    this.submitAttemptAddRowForm = true;
		if(this.addRowForm.value.id != "" && this.addRowForm.value.id){
			if(this.addRowForm.value.prodotto != this.rowProdotto || this.addRowForm.value.quantita != this.rowQuantita || this.oldRowMeasure != this.rowMeasure){
				console.log("VALORI DIVERSI");
				this.rowID = "";
				this.rowProdotto = "";
				this.rowQuantita = "";
				this.rowMeasure = this.firstMeasure;
				this.resetFormAddRowValue();
    		this.submitAttemptAddRowForm = false;
			}
			else{
				console.log("VALORI UGUALI");
				console.log(this.addRowForm.value);
				this.showConfirmAction(this.addRowForm.value);
			}
		}
		else{
			this.rowID = "";
			this.rowProdotto = "";
			this.rowQuantita = "";
			this.rowMeasure = this.firstMeasure;
  		this.submitAttemptAddRowForm = false;
			console.log("OK");
		}
	}
	
	//METODO PER LA CONFERMA DELL'ELIMINAZIONE DI UNA RIGA
	showConfirmAction(value) {
    let confirm = this.alertCtrl.create({
      title: 'CONFERMA ELIMINAZIONE',
      message: 'Sei sicuro di voler eliminare questo articolo?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'SI',
          handler: () => {
						this.deleteRowList(value);
          }
        }
      ]
    });
    confirm.present();
  }

	//METODO PER LA CONFERMA DELL'ELIMINAZIONE DI UNA LISTA
	showConfirmChangeStatusListConfirm(stato,backStatus) {
		let message: any;
		let title: any;
		if(backStatus == "inviata"){
		  message = "Sei sicuro di voler annullare l'invio di questa lista?";
		 	title = 'CONFERMA ANNULLAMENTO INVIO';
		}
		if(backStatus == "pagata"){
		  message = "Sei sicuro di voler annullare il pagamento di questa lista?";
		 	title = 'CONFERMA ANNULLAMENTO PAGAMENTO';
		}
		if(backStatus == 'cancellato'){
		  message = "Sei sicuro di voler annullare la cancellazione di questa lista?";
		 	title = 'CONFERMA ANNULLAMENTO CANCELLAZIONE';
		}
		if(backStatus == 'cancellato-definitivamenre'){
		  message = "Sei sicuro di voler eliminare definitivamente questa lista? Così facendo la lista non potrà mai più essere recuperata!";
		 	title = 'ELIMINAZIONE DEFINITIVA';
		}
    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'SI',
          handler: () => {
						this.showConfirmChangeStatusList(stato,backStatus);
          }
        }
      ]
    });
    confirm.present();
	}
	
	//METODO PER LA CONFERMA DELL'ELIMINAZIONE DI UNA LISTA
	showConfirmChangeStatusList(stato,backStatus) {
		let confirm: any;
		let message: any;
		let title: any;
		if(backStatus == "inviata"){
		  message = "Annullamento invio correttamente effettuato. La lista si trova ora nelle liste aperte ed è visibile nell'APP dell'utente " + this.listNomeUtente + " " + this.listCognomeUtente + ".";
		 	title = 'INVIO ANNULLATO';
		}
		if(backStatus == "pagata"){
		  message = "Pagamento correttamente annullato. La lista si trova ora nell'elenco delle liste inviate.";
		 	title = 'PAGAMENTO ANNULLATO';
		}
		if(backStatus == 'cancellato'){
		  message = "Cancellazione correttamente annullata. La lista si trova ora nell'elenco delle liste inviate.";
		 	title = 'CANCELLAZIONE ANNULLATA';
		}
		if(backStatus == 'cancellato-definitivamenre'){
		  message = "Eliminazione definitiva correttamente effettuata.";
		 	title = 'ELIMINAZIONE DEFINITIVA';
		}
		if(stato == 3){
	    confirm = this.alertCtrl.create({
	      title: 'CONFERMA CANCELLAZIONE',
	      message: 'Sei sicuro di voler cancellare questa lista?',
	      buttons: [
	        {
	          text: 'NO',
	          handler: () => {
	            console.log('Disagree clicked');
	          }
	        },
	        {
	          text: 'SI',
	          handler: () => {
							this.changeListStatus(stato);
	          }
	        }
	      ]
	    });
		}
		if(stato == 2){
	    confirm = this.alertCtrl.create({
	      title: 'CONFERMA PAGAMENTO',
	      message: 'Sei sicuro di voler impostare questa lista come pagata?',
	      buttons: [
	        {
	          text: 'NO',
	          handler: () => {
	            console.log('Disagree clicked');
	          }
	        },
	        {
	          text: 'SI',
	          handler: () => {
							this.changeListStatus(stato);
	          }
	        }
	      ]
	    });
		}
		if(stato == 1){
			if(backStatus){
		    confirm = this.alertCtrl.create({
		      title: title,
		      message: message,
		      buttons: [
		        {
		          text: 'OK',
		          handler: () => {
								this.changeListStatus(stato);
		          }
		        }
		      ]
		    });
			}
			else{
				if(this.listRowDetail != 0 && this.listRowDetail){
			    confirm = this.alertCtrl.create({
			      title: 'CONFERMA INVIO',
			      message: 'Sei sicuro di voler inviare questa lista?',
			      buttons: [
			        {
			          text: 'NO',
			          handler: () => {
			            console.log('Disagree clicked');
			          }
			        },
			        {
			          text: 'SI',
			          handler: () => {
									this.changeListStatus(stato);
			          }
			        }
			      ]
			    });
				}
				else{
			    confirm = this.alertCtrl.create({
			      title: 'LISTA NON INVIATA',
			      message: 'Non è possibile inviare una lista vuota',
			      buttons: [
			        {
			          text: 'OK',
			          handler: () => {
			          }
			        }
			      ]
			    });
				}
			}
		}
		if(stato == 0){
			if(backStatus){
		    confirm = this.alertCtrl.create({
		      title: title,
		      message: message,
		      buttons: [
		        {
		          text: 'OK',
		          handler: () => {
								this.changeListStatus(stato);
		          }
		        }
		      ]
		    });
			}
		}
		if(stato == 4){
			if(backStatus){
		    confirm = this.alertCtrl.create({
		      title: title,
		      message: message,
		      buttons: [
		        {
		          text: 'OK',
		          handler: () => {
								this.changeListStatus(stato);
		          }
		        }
		      ]
		    });
			}
		}
    confirm.present();
  }
	
	/* ---- METODO CHE AGGIUNGE UNA RIGA ---- */
  addRowList(value){
		this.showLoader();
		this.listService.addRowList(value,this.listID).then((result) => {
			this.rowID = "";
			this.rowProdotto = "";
			this.rowQuantita = "";
			this.rowMeasure = this.firstMeasure;
    	this.submitAttemptAddRowForm = false;
			this.getListRowDetail();
		}, (err) => {
		  this.loading.dismiss();
			this.showAlert(err);
		});
  }
	
	//METODO CHE MODIFICA I DATI DI UNA RIGA
	modifyRowList(params){
		this.showLoader();
		this.listService.modifyRowList(params).then((result) => {
			this.rowID = "";
			this.rowProdotto = "";
			this.rowQuantita = "";
			this.rowMeasure = this.firstMeasure;
    	this.submitAttemptAddRowForm = false;
			this.resetFormAddRowValue();
			this.getListRowDetail();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	//METODO CHE ELIMINA UNA DATA RIGA
	deleteRowList(params){
		this.showLoader();
		this.listService.deleteRowList(params).then((result) => {
			this.rowID = "";
			this.rowProdotto = "";
			this.rowQuantita = "";
			this.rowMeasure = this.firstMeasure;
    	this.submitAttemptAddRowForm = false;
			this.showRow = false;
			this.resetFormAddRowValue();
			this.getListRowDetail();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	//METODO CHE CAMBIOA LO STATO DI UNA LISTA CON ID DATO.
	changeListStatus(stato){
		this.showLoader();
		this.listService.changeListStatus(this.listID,stato).then((result) => {
		  this.loading.dismiss();
     	this.navCtrl.pop();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	/* --- METODO CHE RITORNA TUTTE LE MISURE --- */
	getAllMeasure(){
		this.measureService.getAllMeasure(false).then((result) => {
			if(result != 0){
				this.measure = result;
				this.rowMeasure = result[0].Nome;
				this.firstMeasure = result[0].Nome;
			}
			else{
				this.showAlert("404 Nessuna unita di misura trovata");
			}
		}, (err) => {
			this.showAlert("404 Nessuna unita di misura trovata");
		});
	}
	
	//METODO CHE INSERISCE I DATI DI UNA RIGA NEL FORM DI MODIFICA
	insertRowValue(id,prodotto,quantita,measure){
		this.resetFormAddRowValue();
		this.rowID = id;
		this.rowProdotto = prodotto;
		this.rowQuantita = quantita;
		this.rowMeasure = measure;
		this.oldRowMeasure = measure;
		this.showFormRowList = true;
	}
	
	resetFormAddRowValue(){
		this.rowID = "";
		this.rowProdotto = "";
		this.rowQuantita = "";
		this.rowMeasure = this.firstMeasure;
	}
	
	showAdministratorModifyRow(value){
		this.showFormRowList = value;
	}
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		let isErr = false;
		let confirm: any;

		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
			isErr = true;
		}
		else{
			if(err == "404 Nessuna unita di misura trovata"){
				title = "NESSUNA UNITÀ DI MISURA TROVATA";
				content = "Chiedi all'amministratore di creare l'unità di misura desiderata prima di creare un articolo.";
				isErr = false;
			}
			else{
				title = "CONNESSIONE ASSENTE";
				content = "Verifica la connessione a internet";
				isErr = true;
			}
		}
		if(isErr){
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	            this.goHome();
	          }
	        }
	      ]
	    });
		}
		else{
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	          }
	        }
	      ]
	    });
		}
    confirm.present();
  }
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
	
	/* ---- METODO PER TORNARE ALLE LISTE ---- */
	returnListsPage(){
		this.navCtrl.pop();
	}
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailList(){
		this.cambioFunzionalita = "dettagli";
	}
	
	
	//METODO CHE FA VISUALIZZAE I DETTAGLI
	showDetailListAction(){
    if(this.showDetailListButton){
			this.showDetailListButton = false;
		}
		else{
			this.showDetailListButton = true;
		}
	}
	
	//METODO CHE RITORNA IL NUMERO COMPLESSIVO DI PEZZI.
	getNumberRowsArticle(){
		let totalArticles: any = 0;
		for(let row of this.listRowDetail){
			totalArticles = totalArticles + parseFloat(row.Numero);
		};
		return totalArticles;
	}
}
