import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { LoginPage } from '../../login/login';
import { MeasurePage } from '../measure/measure';

import { MeasureProvider } from '../../../providers/measure/measure';

@IonicPage()
@Component({
  selector: 'page-measure-detail',
  templateUrl: 'measure-detail.html',
  providers: [MeasureProvider]
})
export class MeasureDetailPage {
	
	measureID: any;
	measureNome: any;
	measurePredefinita: any;
	
	showStaticPart: any;
	loading: any;
	cambioFunzionalita: string = "dettagli";
	
	modifyMeasureForm: FormGroup;
	submitAttempt: boolean = false;

	submitAttemptAddRowForm: boolean = false;
	rowsNumber: any;
	
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public measureService: MeasureProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App) {
  	this.measureID = navParams.get("measureID");
  	this.measureNome = navParams.get("measureNome");
  	this.measurePredefinita = navParams.get("measurePredefinita");
		
		this.showStaticPart = true;	
		
		this.modifyMeasureForm = formBuilder.group({
			id: [this.measureID],
      measureNome: [this.measureNome, Validators.required],
      measurePredefinita: [this.measurePredefinita, Validators.required]
    });
	}
	
	//METODO PER LA CONFERMA DELL'ELIMINAZIONE DI UNA LISTA
	showConfirmDeleteMeasure(stato,backStatus) {
		let message: any = "Sei sicuro di voler eliminare definitivamente questa unità di misura? Così facendo l'unità di misura non potrà mai più essere recuperata!";
		let title: any = 'CANCELLAZIONE';
    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'SI',
          handler: () => {
						this.deleteMeasure();
          }
        }
      ]
    });
    confirm.present();
	}
	
	/* METODO CHE ELIMINA UN'UNITA DI MISURA */
	deleteMeasure(){
		this.showLoader();
		this.measureService.deleteMeasure(this.measureID).then((result) => {
		  this.loading.dismiss();
			this.returnMeasurePage();
		}, (err) => {
		  this.loading.dismiss();
			this.showAlert(err);
		});
	}
	
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM
	modifyMeasureFormAction(){
    this.submitAttempt = true;
		if(this.modifyMeasureForm.valid){
			this.modifyMeasure(this.modifyMeasureForm.value);
			console.log("OK");
		}
    console.log(this.modifyMeasureForm.value);
	}
	
	/* METODO CHE MODIFICA UN'UNITA DI MISURA */
	modifyMeasure(params){
		this.showLoader();
		this.measureService.modifyMeasure(params).then((result) => {
	  	this.measureNome = params.measureNome;
	  	this.measurePredefinita = params.measurePredefinita;
    	this.submitAttempt = false;
			this.showDetailMeasure();
		  this.loading.dismiss();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		let isErr = false;
		let confirm: any;

		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
			isErr = true;
		}
		else{
			title = "CONNESSIONE ASSENTE";
			content = "Verifica la connessione a internet";
			isErr = true;
		}
		if(isErr){
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	            this.goHome();
	          }
	        }
	      ]
	    });
		}
		else{
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	          }
	        }
	      ]
	    });
		}
    confirm.present();
  }
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
	
	/* ---- METODO PER TORNARE ALLE LISTE ---- */
	returnMeasurePage(){
		this.navCtrl.pop();
	}
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailMeasure(){
		this.cambioFunzionalita = "dettagli";
	}
}
