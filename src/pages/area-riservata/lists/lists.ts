import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { ListsProvider } from '../../../providers/lists/lists';

import { LoginPage } from '../../login/login';
import { ListDetailPage } from '../list-detail/list-detail';

@IonicPage()
@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html',
  providers: [ListsProvider]
})
export class ListsPage {
	
	showStaticPart: any;
	showList: any;
	lists: any;
	numberLists: any;
	filteredLists: any;
	loading: any;
	
	statoLista: any;
	cambioFunzionalita: string = "dettagli";
	
	newListForm: FormGroup;
	submitAttempt: boolean = false;
	role: any;
	ruoloUtente: any;

	showSearchForm: boolean = false;
	searchDataDal: string = '';
	searchDataAl: string = '';
	searchTerm: string = '';
	searchTarga: string = '';
	//searchPagamento: string = 'qualsiasi';
	
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public listService: ListsProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App, public storage: Storage) {
		this.statoLista = navParams.get("stato");
		this.ruoloUtente = navParams.get("userRole");
		
		this.newListForm = formBuilder.group({
      nomeCliente: ["", Validators.required],
      targa: [""],
			//pagamaneto: ["S", Validators.required], RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
      note: [""]
    });
  	this.getRole();
	}

  ionViewWillEnter() {		
		this.showStaticPart = false;
		this.showList = false;
		this.showLoader();
		this.getUserLists(false);
  }

	/* --- METODO CHE RITORNA TUTTE LE LISTE ATTIVE --- */
	getUserLists(openLast){
		this.listService.getUserLists(this.statoLista).then((result) => {
		  this.loading.dismiss();
			if(result != 0){
				this.showList = true;
				this.lists = result;
				this.numberLists = result["length"];
				this.filteredLists = result;
				if(openLast){
					this.openList(this.lists[0].ID,this.lists[0].NomeCliente,this.lists[0].Note,this.lists[0].Targa,this.lists[0].DateDetailShow,this.lists[0].Nome,this.lists[0].Cognome)
				}
			}
			else{
				this.numberLists = 0;
			}
			this.showStaticPart = true;
		}, (err) => {
		  this.loading.dismiss();
			if(err != "404 Nessuna lista trovata"){
			  console.log(err);
				this.showAlert(err);
			}
		});
	}
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM PER AGGIUNGERE UNA LISTA
	newListFormAction(){
    this.submitAttempt = true;
		if(this.newListForm.valid){
			this.addList(this.newListForm.value);
			console.log("OK");
		}
    console.log(this.newListForm.value);
	}
	
	/* --- METODO CHE AGGIUNGE UNA NUOVA LISTA --- */
	addList(value){
		this.showLoader();
		this.listService.addList(value).then((result) => {
			this.newListForm.reset();
    	this.submitAttempt = false;
			this.cambioFunzionalita = "dettagli";
			this.getUserLists(true);
		}, (err) => {
		  this.loading.dismiss();
			this.showAlert(err);
		});
	}

	//METODO CHE APRE LA SINGOLA LISTA
	openList(ID,nomeCliente,note,targa,data,nomeUtente,cognomeUtente){
		this.navCtrl.push(ListDetailPage,{
			listID: ID,
			nomeCliente: nomeCliente,
			note: note,
			statoLista: this.statoLista,
			targa: targa,
			data: data,
			nomeUtente: nomeUtente,
			cognomeUtente:cognomeUtente,
			userRole: this.ruoloUtente
		});
	}
	
	getRole(){
		this.storage.get('role').then((value) => {
			if (value != null){
				this.role = value;
			}
		});
	}
	
	//METODO CHE PERMETTE DI VISUALIZZARE IL FORM FILTRI
	activeFilterForm(){
		if(!this.showSearchForm){
			this.showSearchForm = true
		}
		else{
			this.showSearchForm = false;
		}
	}
	
	//METODO CHE PERMETTE DI SVUOTARE I FILTRI DEL FORM
	clearFilterForm(){
		this.searchDataDal = '';
		this.searchDataAl = '';
		this.searchTerm = '';
		this.searchTarga = '';
		//this.searchPagamento = 'qualsiasi'; RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
		this.filteredLists = this.lists;
	}
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailLists(){
		this.cambioFunzionalita = "dettagli";
	}
	
	/* ---- METODO CHE FILTRA LE LISTE ---- */
	setFilteredItems(){
		this.showList = false;
		this.filteredLists = this.lists;
		if(this.searchTerm != ""){
	    this.filteredLists = this.filteredLists.filter((list) => {
				if(list.NomeCliente && list.NomeCliente != ""){
			  	return list.NomeCliente.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
				}
			});
		}
		/* RIMOSSO PAGAMENTO COME RICHIESTO DAL CLIENTE
		if(this.searchPagamento != "qualsiasi"){
	    this.filteredLists = this.filteredLists.filter((list) => {
				if(list.Pagamento && list.Pagamento != ""){
			  	return list.Pagamento.toLowerCase().indexOf(this.searchPagamento.toLowerCase()) > -1;
				}
			});
		}
		*/
		if(this.searchTarga != ""){
	    this.filteredLists = this.filteredLists.filter((list) => {
				if(list.Targa && list.Targa != ""){
			  	return list.Targa.toLowerCase().indexOf(this.searchTarga.toLowerCase()) > -1;
				}
			});
		}

		if(this.searchDataDal != "" || this.searchDataAl != ""){
			if(this.searchDataDal != ""){
		    this.filteredLists = this.filteredLists.filter((list) => {
					if(list.LastEdited >= this.searchDataDal){
						return list;
					}
				});
			}
			if(this.searchDataAl != ""){
		    this.filteredLists = this.filteredLists.filter((list) => {
					if(list.LastEdited <= this.searchDataAl){
						return list;
					}
				});
			}
		}
		
		if(this.filteredLists && this.filteredLists != 0){
			this.showList = true;
		}
		if(this.searchTerm == "" && this.searchDataAl == "" && this.searchDataDal == ""){
			this.filteredLists = this.lists;
		}
	}
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
		}
		else{
			title = "CONNESSIONE ASSENTE";
			content = "Verifica la connessione a internet";
		}
	  let confirm = this.alertCtrl.create({
      title: title,
      message: content,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.goHome();
          }
        }
      ]
    });
    confirm.present();
  }
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
}