import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController, AlertController, ModalController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { AuthProvider } from '../../../providers/auth/auth';

import { LoginPage } from '../../login/login';
import { UserDetailModalPage } from '../user-detail-modal/user-detail-modal';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
  providers: [AuthProvider]
})
export class UserPage {
	
	showStaticPart: any;
	showUsers: any;
	cambioFunzionalita: string = "utenti";
	users: any;
	loading: any;
	
	createUserForm: FormGroup;
	submitAttempt: boolean = false;
	
  constructor(private formBuilder: FormBuilder, public modalCtrl: ModalController, public navCtrl: NavController, public params: NavParams, public storage: Storage, public authService: AuthProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App) {
		this.showStaticPart = false;
		this.showUsers = false;
		this.showLoader();
		this.getUser();
		this.createUserForm = formBuilder.group({
      nome: ['', Validators.required],
      cognome: ['', Validators.required],
			nomeUtente: ['', Validators.required],
			password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
			ruolo: ['2', Validators.required]
    });
	}

  ionViewDidLoad() {
		if(this.params.get('role') > 1){
			this.goHome();
		}
		this.storage.get('token').then((value) => {
			if (value == null){
				this.goHome();
			}
		});
  }
	
	/* --- METODO CHE RITORNA TUTTI GLI UTENTI REGISTRATI --- */
	getUser(){
		this.authService.getAllUsers().then((result) => {
		  this.loading.dismiss();
			if(result != 0){
				this.showUsers = true;
				this.users = result;
			}
			this.showStaticPart = true;
		}, (err) => {
		  this.loading.dismiss();
			if(err != "404 Nessun utente trovato"){
			  console.log(err);
				this.showAlert(err);
			}
		});
	}
	
	/* ---- LOGOUT ---- */
  logout(){
		this.showLoader();
		this.authService.logout().then((result) => {
		  this.loading.dismiss();
		  this.goHome();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
  }
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }

	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		let isErr = false;
		let confirm: any;
		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
			isErr = true;
		}
		else{
			if(err == "406 Not Acceptable"){
				title = "ERRORE USERNAME";
				content = "Username già utilizzato per un'altro utente, riprovare con un altro username";
			}
			else{
				title = "CONNESSIONE ASSENTE";
				content = "Verifica la connessione a internet";
				isErr = true;
			}
		}
		if(isErr){
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	            this.goHome();
	          }
	        }
	      ]
	    });
		}
		else{
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
							this.getUser();
	          }
	        }
	      ]
	    });
		}
    confirm.present();
  }
	
	//MERODO CHE RITORNA IL RUOLO DELL'UTENTE IN FORMA TESTUALE
	getRoleText(role){
		if(role == 0){
			return "Admin";
		}
		if(role == 1){
			return "Amministratore";
		}
		if(role == 2){
			return "Impiegato";
		}
	}
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
	
	/* ---- METODO CHE VIENE RICHIAMATO UNA VOLTA CONCLUSO IL FORM ---- */
	formAction(){
    this.submitAttempt = true;
		if(this.createUserForm.valid){
			this.addNewUser(this.createUserForm.value);
		}
    //console.log(this.createUserForm.value);
	}
	
	/* ---- METODO CHE AGGIUNGE UN UTENTE ---- */
  addNewUser(value){
		this.showLoader();
		this.authService.addNewUser(value).then((result) => {
			//this.showAlert("Utente inserito con successo");
			this.createUserForm.reset();
    	this.submitAttempt = false;
			this.getUser();
			this.cambioFunzionalita = "utenti";
		}, (err) => {
		  this.loading.dismiss();
			this.showAlert(err);
		});
  }
	
	/* ---- METODO PER APRIRE LA SCHEDA DEL CLIENTE ---- */
	openModal(ID, Nome, Cognome, UserName, Ruolo, Attivo, ListeAperte, ListeInviate, ListePagate, ListeEliminate) {
    let modal = this.modalCtrl.create(UserDetailModalPage, {ID: ID, Nome: Nome, Cognome: Cognome, UserName: UserName, Ruolo: Ruolo, Attivo: Attivo, ListeAperte: ListeAperte, ListeInviate: ListeInviate, ListePagate: ListePagate, ListeEliminate: ListeEliminate});
		modal.onDidDismiss(data => {
			if(data){
				this.users = data;
			}
   	});
		modal.present();
  }
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailUser(){
		this.cambioFunzionalita = "utenti";
	}
}
