import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ViewController, AlertController, LoadingController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { AuthProvider } from '../../../providers/auth/auth';

import { LoginPage } from '../../login/login';

@IonicPage()
@Component({
  selector: 'page-user-detail-modal',
  templateUrl: 'user-detail-modal.html',
  providers: [AuthProvider]
})
export class UserDetailModalPage {
	showStaticPart: any;
  user: any;
  users: any;
	loading: any;
	isMyUser: any;
	cambioFunzionalita: string = "utente";
	
	//PARAMETRI PASSATI
	nome: any;
	cognome: any;
	userName: any;
	ruolo: any;
	stato: any;
	listeAperte: any;
	listeInviate: any;
	listePagate: any;
	listeEliminate: any;
	
	modifyUserForm: FormGroup;
	submitAttempt: boolean = false;
	
  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public params: NavParams, public storage: Storage, public viewCtrl: ViewController, public authService: AuthProvider, public alertCtrl: AlertController, public appCtrl: App, public loadingCtrl: LoadingController) {
		this.showStaticPart = true;
		this.isMyUser = true;
		this.nome = this.params.get('Nome');
		this.cognome = this.params.get('Cognome');
		this.userName = this.params.get('UserName');
		this.ruolo = this.params.get('Ruolo');
		this.stato = this.params.get('Attivo');
		
		this.listeAperte = this.params.get('ListeAperte');
		this.listeInviate = this.params.get('ListeInviate');
		this.listePagate = this.params.get('ListePagate');
		this.listeEliminate = this.params.get('ListeEliminate');
		
		this.setUserValue();
		
		this.verifyUserName();
		this.modifyUserForm = formBuilder.group({
			id: [this.params.get('ID')],
      nome: [this.nome, Validators.required],
      cognome: [this.cognome, Validators.required],
			nomeUtente: [this.userName, Validators.required],
			password: ["",Validators.minLength(8)],
			ruolo: [this.ruolo, Validators.required],
			stato: [this.stato, Validators.required]
    });
  }
	
	//MERODO CHE RITORNA IL RUOLO DELL'UTENTE IN FORMA TESTUALE
	getRoleText(role){
		if(role == 0){
			return "Admin";
		}
		if(role == 1){
			return "Amministratore";
		}
		if(role == 2){
			return "Impiegato";
		}
	}
	
	getUser(){
		this.authService.getAllUsers().then((result) => {
		  this.loading.dismiss();
			if(result != 0){
				this.users = result;
			}
		}, (err) => {
		  this.loading.dismiss();
			if(err != "404 Nessun utente trovato"){
			  console.log(err);
				this.showAlert(err);
			}
		});
	}
	
	//METODO CHE ELIMINA L'UTENTE
	deleteUser(){
		this.showLoader();
		this.authService.deleteUser(this.params.get('ID')).then((result) => {
			if(result == "Utente non eliminabile"){
		  	this.loading.dismiss();
				this.showAlert("Utente non eliminabile");
			}
			else{
				this.showAlert("Utente eliminato con successo");
				this.getUser();
			}
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	//METODO CHE VERIFICA LA CORRETTEZZA DEI DATI INSERITI NEL FORM
	modifyUserFormAction(){
    this.submitAttempt = true;
		if(this.modifyUserForm.valid){
			this.modifyUser(this.modifyUserForm.value);
		}
    //console.log(this.createUserForm.value);
	}
	
	//METODO CHE MODIFICA I DATI DELL'UTENTE
	modifyUser(params){
		this.showLoader();
		this.authService.modifyUser(params).then((result) => {
			//this.showAlert("Utente modificato con successo");
			//this.modifyUserForm.reset();
			this.nome = params.nome;
      this.cognome = params.cognome;
      this.userName = params.nomeUtente;
      this.ruolo = params.ruolo;
			this.stato = params.stato;
			this.setUserValue();
    	this.submitAttempt = false;
			this.getUser();
			this.cambioFunzionalita = "utente";
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
	}
	
	setUserValue(){
		this.user = [
      {
        name: this.nome,
        items: [
          { title: 'Stato', note: this.stato},
          { title: 'Nome', note: this.nome},
          { title: 'Cognome', note: this.cognome},
          { title: 'Nome Utente', note: this.userName},
          { title: 'Password', note: "********"},
          { title: 'Ruolo', note: this.ruolo},
          { title: 'Liste Aperte', note: this.listeAperte},
          { title: 'Liste Inviate', note: this.listeInviate},
          { title: 'Liste Pagate', note: this.listePagate},
          { title: 'Liste Eliminate', note: this.listeEliminate}
        ]
			}
		];
	}
	
	//VERIFY USER
	verifyUserName(){
		this.storage.get('userName').then((value) => {
			if(this.params.get('UserName') != value){
				this.isMyUser = false;
			}
		});
	}
	
	//METODO CHE DOPO LA MODIFICA DELL'UTENTE TI RIPORTA AL DETTAGLIO DELL'UTENTE
	showDetailUser(){
		this.cambioFunzionalita = "utente";
	}
	
	//METODO CHE CHIUDE IL MODAL
  dismiss() {
		if(this.users && this.users != 0){
    	this.viewCtrl.dismiss(this.users);
		}
		else{
    	this.viewCtrl.dismiss();
		}
  }
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }

	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
	
	//METODO PER LA CONFERMA DELL'ELIMINAZIONE DI UN UTENTE
	showConfirmAction() {
    let confirm = this.alertCtrl.create({
      title: 'CONFERMA ELIMINAZIONE',
      message: 'Sei sicuro di voler eliminare questo utente?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'SI',
          handler: () => {
            this.deleteUser();
          }
        }
      ]
    });
    confirm.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		let isErr = false;
		let confirm: any;
		
		if(err == "Utente non eliminabile"){
			title = "ERRORE";
			content = "Questo utente non può essere eliminato perchè ha alcune liste associate.";
		}
		else{
			if(err == "Utente eliminato con successo"){
				title = "UTENTE ELIMINATO";
				content = "Utente eliminato con successo.";
			}
			else{
				if(err == "404 Not Found"){
					title = "SERVIZIO NON DISPONIBILE";
					content = "Servizio al momento non disponibile, riprovare più tardi";
					isErr = true;
				}
				else{
					title = "CONNESSIONE ASSENTE";
					content = "Verifica la connessione a internet";
					isErr = true;
				}
			}
		}
		
		if(isErr){
	    confirm = this.alertCtrl.create({
	      title: title,
	      message: content,
	      buttons: [
	        {
	          text: 'OK',
	          handler: () => {
	            this.goHome();
	          }
	        }
	      ]
	    });
		}
		else{
			if(err == "Utente eliminato con successo"){
		    confirm = this.alertCtrl.create({
		      title: title,
		      message: content,
		      buttons: [
		        {
		          text: 'OK',
		          handler: () => {
								this.dismiss();
		          }
		        }
		      ]
		    });
			}
			else{
		    confirm = this.alertCtrl.create({
		      title: title,
		      message: content,
		      buttons: [
		        {
		          text: 'OK',
		          handler: () => {
		          }
		        }
		      ]
		    });
			}
		}
    confirm.present();
  }

}
