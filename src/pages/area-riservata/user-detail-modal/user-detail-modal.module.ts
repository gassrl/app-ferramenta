import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserDetailModalPage } from './user-detail-modal';

@NgModule({
  declarations: [
    UserDetailModalPage,
  ],
  imports: [
    IonicPageModule.forChild(UserDetailModalPage),
  ],
})
export class UserDetailModalPageModule {}
