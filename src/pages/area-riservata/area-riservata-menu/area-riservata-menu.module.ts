import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AreaRiservataMenuPage } from './area-riservata-menu';

@NgModule({
  declarations: [
    AreaRiservataMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(AreaRiservataMenuPage),
  ],
})
export class AreaRiservataMenuPageModule {}
