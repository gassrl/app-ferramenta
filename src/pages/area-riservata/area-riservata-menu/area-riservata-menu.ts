import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, App, ViewController, AlertController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AuthProvider } from '../../../providers/auth/auth';

import { LoginPage } from '../../login/login';	
import { UserPage } from '../user/user';	
import { ListsPage } from '../lists/lists';	
import { MeasurePage } from '../measure/measure';	

@IonicPage()
@Component({
  selector: 'page-area-riservata-menu',
  templateUrl: 'area-riservata-menu.html',
  providers: [AuthProvider]
})
export class AreaRiservataMenuPage {
  
	loading: any;
	role: any;
	
  constructor(public loadingCtrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public authService: AuthProvider, private viewCtrl: ViewController, public alertCtrl: AlertController, public appCtrl: App, public storage: Storage) {
  	this.getRole();
	}
	
	/* ---- LOGOUT ---- */
  logout(){
		this.showLoader();
		this.authService.logout().then((result) => {
		  this.loading.dismiss();
		  this.goHome();
		}, (err) => {
		  this.loading.dismiss();
		  console.log(err);
			this.showAlert(err);
		});
  }
	
	/* ---- METODO PER TORNARE ALLA HOMEPAGE ---- */
	goHome(){
		this.navCtrl.setRoot(LoginPage);
		this.navCtrl.popToRoot();
	}
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		if(err == "404 Not Found"){
			title = "SERVIZIO NON DISPONIBILE";
			content = "Servizio al momento non disponibile, riprovare più tardi";
		}
		else{
			title = "CONNESSIONE ASSENTE";
			content = "Verifica la connessione a internet";
		}
		
    let confirm = this.alertCtrl.create({
      title: title,
      message: content,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.goHome();
          }
        }
      ]
    });
    confirm.present();
  }
	
	/* ---- LINK OPEN ---- */
  launch(pagina,stato){
		if(pagina == "utenti"){
			this.navCtrl.push(UserPage, {role: this.role});
		}
		
		if(pagina == "measure"){
			this.navCtrl.push(MeasurePage);
		}
		
		if(pagina == "liste"){
			this.navCtrl.push(ListsPage, {stato: stato, userRole: this.role});
		}
  };
	
	/* ---- LOADER ---- */
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	getRole(){
		this.storage.get('role').then((value) => {
			if (value != null){
				this.role = value;
			}
		});
	}
}