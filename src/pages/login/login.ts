import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { App,ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { AreaRiservataMenuPage } from '../area-riservata/area-riservata-menu/area-riservata-menu';	
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthProvider]
})
export class LoginPage {
	
  userName: string;
  password: string;
  loading: any;
	showStaticPart: any;
	
  constructor(public navCtrl: NavController, public authService: AuthProvider, public loadingCtrl: LoadingController, private viewCtrl: ViewController, public alertCtrl: AlertController) {
		this.showStaticPart = false;
  }
	
  ionViewDidLoad() {
		this.showLoader();
		//Check if already authenticated
		this.authService.checkAuthentication().then((res) => {
			this.loading.dismiss();
			this.navCtrl.setRoot(AreaRiservataMenuPage);
		}, (err) => {
			this.showStaticPart = true;
			this.loading.dismiss();
		});
  }

  login(){
		this.showLoader();
		let credentials = {
		  userName: this.userName,
		  password: this.password
		};
		
		if(!this.userName || !this.password){
		  this.loading.dismiss();
			this.showAlert("no credential");
		}
		else{
			this.authService.login(credentials).then((result) => {
			  this.loading.dismiss();
			  console.log(result);
			  this.navCtrl.setRoot(AreaRiservataMenuPage);
			}, (err) => {
			  this.loading.dismiss();
			  console.log(err);
				this.showAlert(err);
			});
		}
  }

	//METODO CHE MOSTRA IL LOADER
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });
    this.loading.present();
  }
	
	/* ---- ALERT ---- */
	showAlert(err){
		let title = "";
		let content = "";
		if(err == "401 Unauthorized"){
			title = "CREDENZIALI ERRATE";
			content = "Email o password sbagliate, verifica le tue credenziali e riprova l'accesso.";
		}
		else{
			if(err == "no credential"){
				title = "CREDENZIALI MANCANTI";
				content = "Username o Password mancanti.";
			}
			else{
				title = "CONNESSIONE ASSENTE";
				content = "Verifica la connessione a internet";
			}
		}
		
    let confirm = this.alertCtrl.create({
      title: title,
      message: content,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            
          }
        }
      ]
    });
    confirm.present();
  }

}
