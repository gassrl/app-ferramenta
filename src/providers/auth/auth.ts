import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Md5 } from 'ts-md5/dist/md5';
import 'rxjs/add/operator/map';	

@Injectable()
export class AuthProvider {

  token: any;
  userName: any;
	dominioRequest: any;
 
  constructor(public http: Http, public storage: Storage) {
		//this.dominioRequest = "192.168.0.72/APPFERRAMENTA";
		this.dominioRequest = "appferramenta.gasweb.it";
  }
	
	//METODO CHE VERIFICA SE SIAMO GIà LOGGATI
	checkAuthentication(){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;

				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let data = {
					  Token: this.token
					};
					
	        this.http.post('http://' + this.dominioRequest + '/user-app/checkToken/', data, headers)
	        .subscribe(res => {
						if(res.json() == "401 Unauthorized"){
		        	reject(res.json());
						}
	          resolve(res);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE PERMETTE IL LOGIN
  login(credentials){
    return new Promise((resolve, reject) => {
		  let headers = new Headers();
      headers.append('Content-Type', 'application/json');
			let password = Md5.hashStr(credentials.password);
			let protectedPassword = this.addStringCharacter(password);
			let data = {
			  UserName: credentials.userName,
			  Password: protectedPassword
			};
	    this.http.post('http://' + this.dominioRequest + '/user-app/loginUser/', data, headers)
      .subscribe(res => {
        let data = res.json();
				if(data == "401 Unauthorized"){
        	reject(data);
				}
				//SETTO IL TOKEN NEL LOCAL STORAGE
				let token = this.addStringCharacter(data[0]);
        this.token = token;
        this.storage.set('token', token);
        this.storage.set('username', data[1]);
        this.storage.set('role', data[2]);
				
				//SETTO L'EMAIL NEL LOCAL STORAGE
        this.userName = credentials.userName;
        this.storage.set('userName', credentials.userName);
        resolve(data);
      }, (err) => {
        reject(err.json());
      });
    });
  }
	
	//METODO CHE PERMETTE IL LOGOUT
  logout(){
    return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				if (value != null){
				  let headers = new Headers();
			    headers.append('Content-Type', 'application/json');
					let data = {
					  Token: value
					};
			    this.http.post('http://' + this.dominioRequest + '/user-app/logout/', data, headers)
			    .subscribe(res => {
			      resolve(res);
			    }, (err) => {
			      reject(err.json());
			    });
				}
				else{
	        reject(false);
				}
			});
    });
  }
	
	//METODO CHE RITORNA TUTTI GLI UTENTI
	getAllUsers(){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let data = {
					  Token: this.token
					};
	        this.http.post('http://' + this.dominioRequest + '/user-app/getAllUsers/', data, headers)
	        .subscribe(res => {
	          let data = res.json();
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE AGGIUNGE UN NUOVO UTENTE
	addNewUser(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');

					let protectedPassword = Md5.hashStr(parametri.password);
					let dataParameter = {
					  Token: this.token,
						Nome: parametri.nome,
						Cognome: parametri.cognome,
						UserName: parametri.nomeUtente,
						Password: protectedPassword,
						Role: parametri.ruolo 
					};
	        this.http.post('http://' + this.dominioRequest + '/user-app/addUser/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE MODIFICA UN UTENTE
	modifyUser(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let protectedPassword: any;
					if(parametri.password != "" && parametri.password){
						protectedPassword = Md5.hashStr(parametri.password);
					}
					else{
						protectedPassword = "";
					}
					let dataParameter = {
					  Token: this.token,
						ID: parametri.id,
						Nome: parametri.nome,
						Cognome: parametri.cognome,
						UserName: parametri.nomeUtente,
						Password: protectedPassword,
						Role: parametri.ruolo,
						Attivo: parametri.stato  
					};

	        this.http.post('http://' + this.dominioRequest + '/user-app/modifyUser/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE ELIMINA UN UTENTE
	deleteUser(id){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: id
					};
	        this.http.post('http://' + this.dominioRequest + '/user-app/deleteUser/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
	}
	
	//METODO CHE RITORNA UNA STRINGA DI length CARATTERI RANDOM
	getRandomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
	}

	//METODO CHE AGGIUNGE 5 CARATTERI A UNA STRINGA IN POSIZIONI DATE.
	addStringCharacter(originalPassword){
		let characterAdd = this.getRandomString(4);
		let position = 1;
		let password = [originalPassword.slice(0, position), characterAdd["3"], originalPassword.slice(position)].join('');
		position = 5;
		password = [password.slice(0, position), characterAdd["2"], password.slice(position)].join('');
		position = 22;
		password = [password.slice(0, position), characterAdd["1"], password.slice(position)].join('');
		position = 30;
		password = [password.slice(0, position), characterAdd["0"], password.slice(position)].join('');
		return password;
	}
}