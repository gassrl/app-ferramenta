import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class MeasureProvider {
	
	token: any;
	dominioRequest: any;

  constructor(public http: Http, public storage: Storage) {
		//this.dominioRequest = "192.168.0.72/APPFERRAMENTA";
		this.dominioRequest = "appferramenta.gasweb.it";
  }
	
	//METODO CHE RITORNA TUTTE LE LISTE PER UN UTENTE
	getAllMeasure(getLast){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let data = {
					  Token: this.token,
						GetLast: getLast
					};
	        this.http.post('http://' + this.dominioRequest + '/measure-app/getAllMeasure/', data, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "404 Nessuna unita di misura trovata"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE AGGIUNGE UN NUOVO UTENTE
	addMeasure(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						Nome: parametri.nome,
						Predefinita: parametri.predefinita
					};
	        this.http.post('http://' + this.dominioRequest + '/measure-app/addMeasure/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	/* METODO CHE ELIMINA UN'UNITA DI MISURA */
	deleteMeasure(id){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: id
					};
	        this.http.post('http://' + this.dominioRequest + '/measure-app/deleteMeasure/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
	}
	
	/* METODO CHE ELIMINA UN'UNITA DI MISURA */
	modifyMeasure(param){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: param.id,
						Nome: param.measureNome,
						Predefinita: param.measurePredefinita
					};
	        this.http.post('http://' + this.dominioRequest + '/measure-app/modifyMeasure/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
	}

}
