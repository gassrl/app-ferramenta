import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class ListsProvider {
	
	token: any;
	dominioRequest: any;

  constructor(public http: Http, public storage: Storage) {
		//this.dominioRequest = "192.168.0.72/APPFERRAMENTA";
		this.dominioRequest = "appferramenta.gasweb.it";
  }

	//METODO CHE RITORNA TUTTE LE LISTE PER UN UTENTE
	getUserLists(stato){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let data = {
					  Token: this.token,
						Stato: stato
					};
	        this.http.post('http://' + this.dominioRequest + '/list-app/getUserLists/', data, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "404 Nessuna lista trovata"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE AGGIUNGE UN NUOVO UTENTE
	addList(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						NomeCliente: parametri.nomeCliente,
						Note: parametri.note,
						Pagamento: parametri.pagamaneto,
						Targa: parametri.targa,
						Stato: 0
					};
	        this.http.post('http://' + this.dominioRequest + '/list-app/addList/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }

	//METODO CHE RITORNA TUTTE LE RIGHR PER UNA DATA LISTA
	getListRowsDetail(ListID){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let data = {
					  Token: this.token,
						ListID: ListID
					};
	        this.http.post('http://' + this.dominioRequest + '/list-row-app/getListRowDetail/', data, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "404 Nessuna riga trovata"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE AGGIUNGE UN NUOVO UTENTE
	addRowList(parametri,listID){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ListID: listID,
						Prodotto: parametri.prodotto,
						Quantita: parametri.quantita,
						UnitOfMeasure: parametri.measure
					};
	        this.http.post('http://' + this.dominioRequest +'/list-row-app/addRow/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	//METODO CHE MODIFICA I DATI DI UNA LISTA
	modifyList(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: parametri.id,
						NomeCliente: parametri.nomeCliente,
						Pagamento: parametri.pagamaneto,
						Targa: parametri.targa,
						Note: parametri.note
					};
	        this.http.post('http://' + this.dominioRequest + '/list-app/modifyList/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	modifyRowList(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: parametri.id,
						Prodotto: parametri.prodotto,
						Quantita: parametri.quantita,
						UnitOfMeasure: parametri.measure
					};
	        this.http.post('http://' + this.dominioRequest + '/list-row-app/modifyRowList/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	deleteRowList(parametri){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: parametri.id
					};
	        this.http.post('http://' + this.dominioRequest + '/list-row-app/deleteRowList/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
	
	changeListStatus(id,stato){
    return new Promise((resolve, reject) => {
      //Load token if exists
      this.storage.get('token').then((value) => {
				if (value != null){
	        this.token = value;
				  let headers = new Headers();
		      headers.append('Content-Type', 'application/json');
					let dataParameter = {
					  Token: this.token,
						ID: id,
						Stato: stato
					};
	        this.http.post('http://' + this.dominioRequest + '/list-app/changeListStatus/', dataParameter, headers)
	        .subscribe(res => {
	          let data = res.json();
						if(data == "406 Not Acceptable"){
	          	reject(data);
						}
						resolve(data);
	        }, (err) => {
          	reject(err.json());
	        });
				}
				else{
          reject(false);
				}
      });         
    });
  }
}
