import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { AreaRiservataMenuPage } from '../pages/area-riservata/area-riservata-menu/area-riservata-menu';
import { UserPage } from '../pages/area-riservata/user/user';
import { UserDetailModalPage } from '../pages/area-riservata/user-detail-modal/user-detail-modal';
import { ListsPage } from '../pages/area-riservata/lists/lists';
import { ListDetailPage } from '../pages/area-riservata/list-detail/list-detail';
import { MeasurePage } from '../pages/area-riservata/measure/measure';
import { MeasureDetailPage } from '../pages/area-riservata/measure-detail/measure-detail';

import { AuthProvider } from '../providers/auth/auth';
import { ListsProvider } from '../providers/lists/lists';
import { MeasureProvider } from '../providers/measure/measure';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
		AreaRiservataMenuPage,
		UserPage,
		UserDetailModalPage,
		ListsPage,
		ListDetailPage,
		MeasurePage,
		MeasureDetailPage
  ],
  imports: [
    BrowserModule,
		HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
		AreaRiservataMenuPage,
		UserPage,
		UserDetailModalPage,
		ListsPage,
		ListDetailPage,
		MeasurePage,
		MeasureDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ListsProvider,
    MeasureProvider
  ]
})
export class AppModule {}
